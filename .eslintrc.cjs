/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution');

module.exports = {
	"root": true,
	"extends": [
		"plugin:vue/vue3-essential",
		"eslint:recommended",
		"@vue/eslint-config-typescript"
	],
	"parserOptions": {
		"ecmaVersion": "latest",
		"project": "./tsconfig.json",
		"parser": "@typescript-eslint/parser"
	},
	"plugins": ["only-warn"],
	"ignorePatterns": [".eslintrc.cjs", "vite.config.ts", "src/**/__tests__/*"],
	"rules": {
		"vue/multi-word-component-names": "off",
		"curly": ["warn", "all"],
		"semi": ["warn", "always"],
		"@typescript-eslint/no-unsafe-return": "error",
		"@typescript-eslint/no-unsafe-assignment": "error"
	}
};
