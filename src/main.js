import { createApp } from 'vue';
import App from '@/components/App.vue';
import VueRouter from '@/router';
import VueKonva from 'vue-konva';
import registerElementResizeDirective from '@/lib/vue/elementResizeDirective';

import 'bootstrap/dist/css/bootstrap.min.css'; // Site-wide styling implications, so include here
import "@/assets/main.css";
import "@/assets/menutoggle.css";
