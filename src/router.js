import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/components/Home.vue';
import Play from '@/components/Play/Play.vue';
import EventsDemo from '@/components/EventsDemo.vue';
import HexTest from '@/components/HexTest.vue';
import Counter from '@/components/Counter.vue';
import Weather from '@/components/Weather.vue';
import DrawCanvasExample from '@/components/DrawCanvasExample.vue';
import NotFound from '@/components/NotFound.vue';
import LayoutBare from '@/components/Layout/LayoutBare.vue';
import LayoutPadded from '@/components/Layout/LayoutPadded.vue';
import LayoutBoxed from '@/components/Layout/LayoutBoxed.vue';
